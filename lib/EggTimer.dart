import 'package:flutter/material.dart';

class EggTimer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return EggTimerPage();
  }
}

class EggTimerPage extends StatefulWidget {
  @override
  _EggTimerPageState createState() => _EggTimerPageState();
}

class _EggTimerPageState extends State<EggTimerPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Egg Timer"),
      ),
    );
  }
}
